<?php 

function getPlanetByTerrain(string $terrainType): string
{
    $planets = json_decode(file_get_contents("https://swapi.dev/api/planets/"))->results;
    $planetByTerrain = "";
    foreach ($planets as $key => $planet) 
    {
        if (strpos(strtolower($planet->terrain), strtolower($terrainType)) !== false) 
        {
            if (empty($planetByTerrain))
                $planetByTerrain = $planet;
            else 
                $planetByTerrain = (int) $planetByTerrain->population > (int) $planet->population ? $planetByTerrain : $planet;
        }      
    }
    return empty($planetByTerrain) ? 
        "No se encontró un planeta con las condiciones indicadas" : 
        $planetByTerrain->name; 
}
var_dump(getPlanetByTerrain('ocean'));
