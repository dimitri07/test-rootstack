<?php
function shipMeetConditions(int $amountPassengers, $ship): bool
{
    if ((int) $ship->passengers < $amountPassengers)
        return false;
    
    $time = trim(substr($ship->consumables, 0, strpos($ship->consumables, " ")));
    $time = (int) $time;
    $unityTime = trim(substr($ship->consumables,strpos($ship->consumables, " ")));
    if ($unityTime === 'days' &&  $time < 7) 
        return false;
        
    // belong to original trilogy
    foreach ($ship->films as $key => $film) 
    {
        $film = substr(substr($film, -2), 0, 1);
        $film = (int) $film;
        if ($film >= 4 && $film <= 6)
            return true;
    }
    return false;
}


function fastestShip(int $amountPassengers): string
{
    $starShips = json_decode(file_get_contents("https://swapi.dev/api/starships/"))->results;
    $mostFastShip = "";
    foreach ($starShips as $key => $ship) 
    {
        if (shipMeetConditions($amountPassengers, $ship)) 
        {
            if (empty($mostFastShip))
                $mostFastShip = $ship;
            else 
                $mostFastShip = (float) $ship->max_atmosphering_speed > (float) $mostFastShip->max_atmosphering_speed ? $ship : $mostFastShip; 
        }
    }
    return empty($mostFastShip) ? 
        "No se encontró una nave con las caracteristicas indicadas" : 
        $mostFastShip->name;
}
