<?php

function fetchAndCount(int $amountPersons): array
{
    $allFullNames = '';
    for ($i=0; $i < $amountPersons; $i++) 
    { 
        $user = (json_decode(file_get_contents("https://randomuser.me/api/")))->results;
        $allFullNames .= $user[0]->name->first . $user[0]->name->last;
    }
    $analizeString = count_chars(strtolower($allFullNames), 1);
    $moreRepeated = [ 'char' => '', 'nTimes' => 0];
    foreach ($analizeString as $key => $value)
    {
        if (($key !== 32) && ($moreRepeated['nTimes'] < $value))
            $moreRepeated = ['char' => chr($key), 'nTimes' => $value];
    }
    return $moreRepeated;
}

$charMoreRepeated = fetchAndCount(5);
echo "La letra con mayor número de repeticiones es la: " . $charMoreRepeated['char'] . " con " . $charMoreRepeated['nTimes'] . " repeticiones";  