<?php

function fetchAndFind(int $age): array
{
    $isOlderAge = false;
    do {
        $user = (json_decode(file_get_contents("https://randomuser.me/api/")))->results;
        if ((int) $user[0]->dob->age > $age) 
            $isOlderAge = true;

    } while (!$isOlderAge);
    return $user;
}

// show result
var_dump(fetchAndFind(45));

