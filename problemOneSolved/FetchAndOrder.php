<?php 

class FetchAndOrder{

    const SOURCE_DATA= "https://randomuser.me/api/";
    private $amountUsers;
    private $containerUsers; 

    public function __construct(int $amountUsers)
    {
        $this->amountUsers = $amountUsers;
        $this->containerUsers = []; 
    }

    public function fetchUsers()
    {
        for ($i= 0; $i < $this->amountUsers; $i++) 
            $this->containerUsers[] = (json_decode(file_get_contents(self::SOURCE_DATA)))->results;
    }

    public function orderUsers()
    {
        usort($this->containerUsers, 'self::comparison');
    }

    function comparison($a, $b)
    {
        return strcmp($a[0]->name->first, $b[0]->name->first);
    }

    function listUsers()
    {
        return $this->containerUsers;
    }
}

$users = new FetchAndOrder(5);
$users->fetchUsers();
$users->orderUsers();
$result =  $users->listUsers();
// You can show result, (optional)
var_dump($result);
